#!/usr/bin/env python3

from distutils.core import setup

setup(name='sortie archiver',
      version='1.0',
      description='Archiver to store historical Divvy bike data',
      author='Milad Fatenejad',
      author_email='icksa1@gmail.com',
      scripts=['bin/archiver']
     )
