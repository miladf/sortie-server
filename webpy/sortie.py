import web
import sqlite3
import json
import urllib2

from sortieconf import divvy_data_db

urls = (
    '/stationlist', 'StationList',
    '/history/(.*)', 'History',
    '/divvy', 'Divvy'
    )

class StationList(object):
    def GET(self):
        conn = sqlite3.connect(divvy_data_db)

        ids = []
        cursor = conn.cursor()
        for row in cursor.execute("select id from station_info"):
            ids.append(row[0])

        conn.close()

        web.header('Access-Control-Allow-Origin', '*')
        web.header('Content-Type', 'application/json')
        return json.dumps(ids)

class History(object):
    def GET(self, name):
        parts = name.split("&")
        input_dict = {}
        for part in parts:
            key,value = part.split("=")
            input_dict[key] = value

        station_id = input_dict["id"]
        start_time = input_dict["start"]
        end_time = input_dict["end"]

        conn = sqlite3.connect(divvy_data_db)
        cursor = conn.cursor()

        query_string = "select * from station_%d where time >= ? and time <= ?" % int(station_id)

        result = []
        for row in cursor.execute(query_string,(start_time, end_time)):
            result.append(row)
                                  
        conn.close()

        web.header('Access-Control-Allow-Origin', '*')
        web.header('Content-Type', 'application/json')
        return json.dumps(result)

# This API can be used 2 ways:
# 1. XMLHTTPRequest via http://sortieapp.com:80/sortie/divvy
# 2. JSONP via http://sortieapp.com:80/sortie/divvy?callback=yourJavaScriptCallback
class Divvy(object):
    def GET(self):
        # get GET/POST URL parameters
        defaultCallbackString = "nocallbackatall"
        user_data = web.input(callback = defaultCallbackString)
        userJSCallback = user_data.callback

        divvy = urllib2.urlopen("http://divvybikes.com/stations/json").read()

        if userJSCallback == defaultCallbackString:
            web.header('Access-Control-Allow-Origin', '*')
            web.header('Content-Type', 'application/json')
            return divvy
        else:
            return userJSCallback + "(" + divvy + ")"

if __name__ == "__main__": 
    print "divvy_data_db =", divvy_data_db
    application = web.application(urls, globals())
    application.run()
else:
    application = web.application(urls, globals()).wsgifunc()    
